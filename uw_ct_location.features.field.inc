<?php

/**
 * @file
 * uw_ct_location.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function uw_ct_location_field_default_fields() {
  $fields = array();

  // Exported field: 'node-uw_location-field_location_acronym'.
  $fields['node-uw_location-field_location_acronym'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_location_acronym',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(
        'max_length' => '255',
      ),
      'translatable' => '0',
      'type' => 'text',
    ),
    'field_instance' => array(
      'bundle' => 'uw_location',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => 1,
        ),
        'entity_teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'ical' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_location_acronym',
      'label' => 'Acronym',
      'required' => 0,
      'settings' => array(
        'exclude_cv' => FALSE,
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'size' => '10',
        ),
        'type' => 'text_textfield',
        'weight' => 0,
      ),
    ),
  );

  // Exported field: 'node-uw_location-field_location_details'.
  $fields['node-uw_location-field_location_details'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_location_details',
      'foreign keys' => array(),
      'indexes' => array(
        'lid' => array(
          0 => 'lid',
        ),
      ),
      'locked' => '0',
      'module' => 'location_cck',
      'settings' => array(
        'gmap_macro' => '[gmap ]',
        'gmap_marker' => 'drupal',
        'location_settings' => array(
          'display' => array(
            'hide' => array(
              'additional' => 0,
              'city' => 0,
              'coords' => 0,
              'country' => 0,
              'country_name' => 0,
              'locpick' => 0,
              'map_link' => 0,
              'name' => 0,
              'postal_code' => 0,
              'province' => 0,
              'province_name' => 0,
              'street' => 0,
            ),
          ),
          'form' => array(
            'fields' => array(
              'additional' => array(
                'collect' => '1',
                'default' => '',
                'weight' => '-99',
              ),
              'city' => array(
                'collect' => '1',
                'default' => '',
                'weight' => '-97',
              ),
              'country' => array(
                'collect' => '1',
                'default' => 'ca',
                'weight' => '-94',
              ),
              'locpick' => array(
                'collect' => '1',
                'weight' => '-93',
              ),
              'name' => array(
                'collect' => '1',
                'default' => '',
                'weight' => '-100',
              ),
              'postal_code' => array(
                'collect' => '1',
                'default' => '',
                'weight' => '-95',
              ),
              'province' => array(
                'collect' => '1',
                'default' => '',
                'weight' => '-96',
              ),
              'street' => array(
                'collect' => '1',
                'default' => '',
                'weight' => '-98',
              ),
            ),
          ),
        ),
      ),
      'translatable' => '0',
      'type' => 'location',
    ),
    'field_instance' => array(
      'bundle' => 'uw_location',
      'default_value' => array(
        0 => array(
          'location_settings' => array(
            'display' => array(
              'hide' => array(
                'additional' => 0,
                'city' => 0,
                'coords' => 0,
                'country' => 0,
                'country_name' => 0,
                'locpick' => 0,
                'map_link' => 0,
                'name' => 0,
                'postal_code' => 0,
                'province' => 0,
                'province_name' => 0,
                'street' => 0,
              ),
            ),
            'form' => array(
              'fields' => array(
                'additional' => array(
                  'collect' => '1',
                  'default' => '',
                  'weight' => '-99',
                ),
                'city' => array(
                  'collect' => '1',
                  'default' => '',
                  'weight' => '-97',
                ),
                'country' => array(
                  'collect' => '1',
                  'default' => 'ca',
                  'weight' => '-94',
                ),
                'delete_location' => array(
                  'default' => FALSE,
                  'nodiff' => TRUE,
                ),
                'is_primary' => array(
                  'default' => 0,
                ),
                'latitude' => array(
                  'default' => 0,
                ),
                'lid' => array(
                  'default' => FALSE,
                ),
                'locpick' => array(
                  'collect' => '1',
                  'default' => FALSE,
                  'nodiff' => TRUE,
                  'weight' => '-93',
                ),
                'longitude' => array(
                  'default' => 0,
                ),
                'name' => array(
                  'collect' => '1',
                  'default' => '',
                  'weight' => '-100',
                ),
                'postal_code' => array(
                  'collect' => '1',
                  'default' => '',
                  'weight' => '-95',
                ),
                'province' => array(
                  'collect' => '1',
                  'default' => '',
                  'weight' => '-96',
                ),
                'source' => array(
                  'default' => 0,
                ),
                'street' => array(
                  'collect' => '1',
                  'default' => '',
                  'weight' => '-98',
                ),
              ),
            ),
          ),
          'name' => '',
          'street' => '200 University Ave West',
          'additional' => '',
          'city' => 'Waterloo',
          'province' => 'Ontario',
          'postal_code' => 'N2L 3G1',
          'country' => 'ca',
          'locpick' => array(
            'user_latitude' => '',
            'user_longitude' => '',
          ),
        ),
      ),
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'location_cck',
          'settings' => array(),
          'type' => 'location_default',
          'weight' => 0,
        ),
        'entity_teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'ical' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_location_details',
      'label' => 'Location details',
      'required' => 0,
      'settings' => array(
        'exclude_cv' => FALSE,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'location_cck',
        'settings' => array(),
        'type' => 'location',
        'weight' => '1',
      ),
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Acronym');
  t('Location details');

  return $fields;
}
