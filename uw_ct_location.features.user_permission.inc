<?php

/**
 * @file
 * uw_ct_location.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_location_user_default_permissions() {
  $permissions = array();

  // Exported permission: create uw_location content.
  $permissions['create uw_location content'] = array(
    'name' => 'create uw_location content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete any uw_location content.
  $permissions['delete any uw_location content'] = array(
    'name' => 'delete any uw_location content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: delete own uw_location content.
  $permissions['delete own uw_location content'] = array(
    'name' => 'delete own uw_location content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit any uw_location content.
  $permissions['edit any uw_location content'] = array(
    'name' => 'edit any uw_location content',
    'roles' => array(),
    'module' => 'node',
  );

  // Exported permission: edit own uw_location content.
  $permissions['edit own uw_location content'] = array(
    'name' => 'edit own uw_location content',
    'roles' => array(),
    'module' => 'node',
  );

  return $permissions;
}
