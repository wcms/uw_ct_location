<?php

/**
 * @file
 * Code for the Location feature.
 */

include_once 'uw_ct_location.features.inc';

/**
 * Implements hook_menu().
 */
function uw_ct_location_menu() {
  $items['location/json'] = array(
    'title' => 'Location Data',
    'page callback' => 'uw_ct_location_ajax',
    'access callback' => TRUE,
  );

  return $items;
}

/**
 * Returns either a list of locations, or specific location data given a location node ID.
 */
function uw_ct_location_ajax($action = 'list') {

  switch ($action) {
    case 'list':

      $locations = array();

      // Check if we have the data cached already.
      $cache = cache_get('uw_ct_event:locations', 'cache');
      if ($cache) {
        $locations = $cache->data;
      }
      else {
        // Get the data and create the cache.
        $result = db_query("SELECT {node}.nid FROM {node} WHERE {node}.status = 1 AND {node}.type = 'uw_location' ORDER BY {node}.title ASC");
        foreach ($result as $record) {
          $node = node_load($record->nid);
          if (!empty($node)) {
            $node->field_location_details['und'][0]['name'] = $node->title;
            $locations[] = $node->field_location_details['und'][0];
          }
        }
        cache_set('uw_ct_event:locations', $locations, 'cache', CACHE_TEMPORARY);
      }

      if (!empty($locations)) {
        drupal_json_output($locations);
      }
      else {
        drupal_json_output(
          array(
            'error' => 'ERROR - no location list',
            'description' => "No location data exists on this server.",
          )
        );
      }
      break;

  }

}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Altering the feeds_import_form when importing uw_feed_building_data.
 */
function uw_ct_location_form_feeds_import_form_alter(&$form, &$form_state, $form_id) {
  if ($form['#importer_id'] == 'uw_feed_building_data') {
    global $base_path;
    $form['feeds']['FeedsHTTPFetcher']['source']['#default_value'] = $base_path . drupal_get_path('module', 'uw_ct_location') . '/data/locations.xml';
  }
}
