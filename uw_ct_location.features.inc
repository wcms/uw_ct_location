<?php

/**
 * @file
 * uw_ct_location.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_location_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function uw_ct_location_node_info() {
  $items = array(
    'uw_location' => array(
      'name' => t('Location'),
      'base' => 'node_content',
      'description' => t('Content type used to generate event locations and populate location details'),
      'has_title' => '1',
      'title_label' => t('Location'),
      'help' => '',
    ),
  );
  return $items;
}
